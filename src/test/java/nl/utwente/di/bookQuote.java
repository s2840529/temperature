package nl.utwente.di;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class bookQuote {
	@Test
	public void testCelcius() throws Exception {
		Converter conv = new Converter();
		double f = conv.getCtoF(-30.0);
		Assertions.assertEquals(-22.0, f, 0.0, "C to f");

	}
}
