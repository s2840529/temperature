package nl.utwente.di;

public class Converter {
	public Converter() {
	}

	public double getCtoF(double c) {
		return c * (9.0 / 5.0) + 32;
	}
}
